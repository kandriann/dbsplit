## DBSplit
DBSplit is a Java plugin that splits in two halves a SQL-based relational database given in input, according to some split criteria. The split is performed horizontally, which means that each table in the database will be split in terms of records, thus preserving its schema. 

The split is guided by two main parameters: the **split percentage**, establishing the number of records belonging to each table of the two halves databases that will be generated in output (e.g., 0% means that one half is empty, 50% means halves have same size), and the **overlapping percentage**, establishing the number of records in common in each table of the two halves (e.g., 0% means complete disjoint halves, 100% means equivalent halves). 

DBSplit can be integrated within a test generator tool, by using one half database as input data provider for test cases and the other half to configure the initial state of the application under test. This can help in differentiating test cases and covering a large amount of scenarios (e.g., if two completely disjoint halves are employed, the test cases can potentially use data that have never been used by the application under test).


## How to use DBSplit 
The following arguments are expected to run DBSplit (i.e., via compiled main class or produced jar file): `<server> <user> <password> <input-db> <output-db1> <output-db2> <perc-split> <split-type> <perc-overlapping> <dbms>`.

- **server**: the  DBMS server name and port to connect to. Leave it empty in case of SQLite;
- **user**: the DBMS server user. Leave it empty in case of SQLite;
- **password**: the DBMS server password. Leave it empty in case of SQLite;
- **input-db**: the database to split. It must refer to an existing database in MySQL/SQL Server DBMS or to the path to an existing SQLite database;
- **output-db1**: the database first half produced in output. It must refer to a non-existing database in MySQL/SQL Server DBMS or to the path to a non-existing SQLite database;
- **output-db2**: the database second half given in output. It must refer to a non-existing database in MySQL/SQL Server DBMS or to the path to a non-existing SQLite database;
- **perc-split**: the split %, between _0_ and _100_;
- **split-type**: the split type, either _disjoint_ or _overlapping_;
- **perc-overlapping**: the overlapping %, between _0_ and _100_. It is only needed if **split-type** is set to _overlapping_;
- **dbms**: the DBMS server type. Currently supported DBMS are _mysql_, _sqlserver_, and _sqlite_; 

Example (once a Jar file is created): `java -jar dbsplit.jar localhost:3306 user001 password001 testDB half1DB half2DB 30 overlapping 60 mysql`
