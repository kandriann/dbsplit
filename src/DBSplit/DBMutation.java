package DBSplit;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class DBMutation {
	
	private static Scanner tastiera = new Scanner(System.in).useDelimiter("\n");
	private static String risp="";
	public static boolean run;
	private static String query="";
	private static Statement st = null;
	private static ResultSet rs = null;
	private static String val = "";
	private static String tipo = "";
	private static String cond = "";
	
	//Metodo iniziale, scelta della tabella, colonna e l'operazione da eseguire (blank o modify)
	public static void mutation(String db) {
		String [] index = null;
		String tabella = "";
		String colonna = "";
		String data_type = "";
		String op = "";
		int size;
		risp="si";
		while(risp.equalsIgnoreCase("si")) {
			System.out.println("Tabella:");
			tabella = tastiera.nextLine();
			if(searchTable(tabella, db)) {
				//crea array index
				try {
					DatabaseMetaData dbm = DBConnection.getConn().getMetaData();
					rs = dbm.getIndexInfo(null, null, tabella, false, false);
					if(rs != null) {
						rs.last();
						size = rs.getRow();
						rs.beforeFirst();
						index = new String[size];
						int i = 0;
						while(rs.next()) {
							index[i] = rs.getString("COLUMN_NAME");
							i++;
						}
					}
				}catch(SQLException se) {
					System.out.println("Error: "+se.getMessage());
				}finally {
					DBConnection.closeRs(rs);
				}
				//inserimento colonna su cui eseguire l'operazione
				while(risp.equalsIgnoreCase("si")) {
					System.out.println("Colonna (non key/index):");
					colonna = tastiera.nextLine();
					System.out.println("Data type: (Numeric - String - Altro)");
					data_type = tastiera.nextLine();
					if(searchColumn(colonna, tabella, db) && !isKey(colonna,index)
							&& (data_type.equalsIgnoreCase("string")|| data_type.equalsIgnoreCase("altro")
									|| data_type.equalsIgnoreCase("numeric"))) {
						//scelta operazione (blank-modify)
						System.out.println("Inserici quale operazione eseguire (blank - modify):");
						op = tastiera.nextLine();
						if(op.equalsIgnoreCase("blank")) {
							blank(colonna,data_type,tabella);								
						}else if(op.equalsIgnoreCase("modify")) {
							modify(colonna,data_type,tabella);								
						}else {
							System.out.println("Errore, operazione inserita non valida");
						}								
					}else {
						System.out.println("Errore input non valido");
					}					
					System.out.println("Eseguire altre modifiche alla tabella: " +tabella+"?(si/no)");
					risp = risposta();				
				}			
				System.out.println("Eseguire altre modifiche al db:" +db+"?(si/no)");
				risp = risposta();				
			}else {
				System.out.println("Riprovare? (si/no)");
				risp = risposta();
			}	
		}		
		System.out.println("Fine modifiche");
	}
			
	/*Metodo per il riempimento dei blank
	 * - conta il numero di blank presenti nella colonna (se non presenti finisce l'esecuzione)
	 * - scelta tipologia di riempimento
	 * - invocazione del rispettivo metodo
	 */ 
	public static void blank(String colonna, String data_type, String tabella) {
		int n_blank = -1;
		if(data_type.equalsIgnoreCase("string")) {
			query = "SELECT COUNT(*) FROM " + Main.DB+"." +tabella+ " WHERE " +colonna+" IS NULL OR " +colonna+ "=''";
		}else {
			query = "SELECT COUNT(*) FROM " + Main.DB+"." +tabella+ " WHERE " +colonna+" IS NULL";
		}
		try {
			st = DBConnection.getConn().createStatement();
			rs = st.executeQuery(query);
			if(rs.next()) {
				n_blank = rs.getInt(1);
			}
		}catch(SQLException se) {
			System.out.println("Error: "+se.getMessage());
		}finally {
			DBConnection.closeSt(st);
			DBConnection.closeRs(rs);
		}
		if(n_blank == 0) {
			System.out.println("Non sono presenti spazi vuoti in: "+colonna);
		}else if(n_blank > 0) {
			while(risp.equalsIgnoreCase("si")) {
				System.out.println("Inserire riempimento (average - input - random)");
				tipo = tastiera.nextLine();
				if(tipo.equalsIgnoreCase("average")) {
					average(colonna,data_type,tabella);
					risp="no";
				}else if(tipo.equalsIgnoreCase("input")) {
					inputB(colonna,data_type,tabella);
					risp="no";
				}else if(tipo.equalsIgnoreCase("random")) {
					random(colonna,data_type,tabella);
					risp="no";
				}else {
					System.out.println("Tipo inserito non valido, riprovare? (si/no)");
					risp = risposta();
				}
			}
		}						
		System.out.println("Fine, blank");
	}
	
	/*Metodo per riepimento: AVERAGE
	 * - se la colonna � di tipo numerico, riempie con la media
	 * - se la colonna � di tipo stringa/altro, riempie con la pi� frequente
	 */
	public static void average(String colonna, String data_type,String tabella) {
		if(data_type.equalsIgnoreCase("numeric")) {
			try {
				query = "SELECT AVG("+colonna+") FROM " + Main.DB+"." +tabella;
				st = DBConnection.getConn().createStatement();
				rs = st.executeQuery(query);
				if(rs.next()) {				
					int avg = rs.getInt(1);
					query = "UPDATE " +tabella+ " SET "+colonna+"= " +avg+ " WHERE " +colonna+" IS NULL";
					st.executeUpdate(query);
					System.out.println("Operazione eseguita, blank riempiti con: " +avg);
				}			
			}catch(SQLException se) {
				System.out.println("Error: " + se.getMessage());
			}finally {
				DBConnection.closeSt(st);
				DBConnection.closeRs(rs);
			}
		}else{
			try {
				query = "SELECT " +colonna+ ", COUNT("+colonna+") AS myCount FROM " + Main.DB+"."+ tabella+ " WHERE "+colonna+" IS NOT NULL AND " +colonna+ "!= '' GROUP BY " +colonna+ " ORDER BY myCount DESC";
				st = DBConnection.getConn().createStatement();
				rs = st.executeQuery(query);
				if(rs.next()) {
					if(data_type.equalsIgnoreCase("string")) {
						val = rs.getString(1);
						query = "UPDATE " +tabella+ " SET "+colonna+"= '" +val+ "' WHERE " +colonna+" IS NULL OR " +colonna+ "= ''" ;
					}else if (data_type.equalsIgnoreCase("altro")) {
						val = rs.getObject(1).toString();
						query = "UPDATE " +tabella+ " SET "+colonna+"= '" +val+ "' WHERE " +colonna+" IS NULL";
					}	
					st = DBConnection.getConn().createStatement();
					st.executeUpdate(query);
					System.out.println("Operazione eseguita, blank riempiti con: "+val);
				}
			}catch(SQLException se) {
				System.out.println("Error: " + se.getMessage());
			}finally {
				DBConnection.closeSt(st);
				DBConnection.closeRs(rs);
			}	
		}
	}
	
	/*Metodo per riepimento: INPUT
	 * - chiede il valore all'utente
	 * - riempie i blank con il valore indicato
	 */
	public static void inputB(String colonna, String data_type,String tabella) {
		System.out.println("Inserisci il valore:");
		val = tastiera.nextLine();
		if(data_type.equalsIgnoreCase("string")) {
			query = "UPDATE " +tabella+ " SET "+colonna+"= '" +val+ "' WHERE " +colonna+" IS NULL OR " +colonna+ "= ''" ;
		}else {
			query = "UPDATE " +tabella+ " SET "+colonna+"= '" +val+ "' WHERE " +colonna+" IS NULL" ;
		}
		try {
			st = DBConnection.getConn().createStatement();
			st.executeUpdate(query);
			System.out.println("Operazione eseguita, blank riempiti con: "+val);
		}catch(SQLException se) {
			System.out.println("Error: " + se.getMessage());
		}finally {
			DBConnection.closeSt(st);
		}		
	}
	
	/*Metodo per riepimento: RANDOM
	 * - prende un valore random presente nel database (della colonna su cui si sta operando)
	 * - riempie i blank con quel valore
	 */
	public static void random(String colonna, String data_type, String tabella) {
		try {
			query = "SELECT "+colonna+ " FROM " + Main.DB+"."+ tabella+ " WHERE "+colonna+" IS NOT NULL AND "+colonna+"!='' ORDER BY RAND() LIMIT 1";
			st = DBConnection.getConn().createStatement();
			rs = st.executeQuery(query);
			if(rs.next()) {				
				val = rs.getObject(1).toString();
				if(data_type.equalsIgnoreCase("string")) {
					query = "UPDATE " +tabella+ " SET "+colonna+"= '" +val+ "' WHERE " +colonna+" IS NULL OR "+ colonna + "= ''";
				}else {
					query = "UPDATE " +tabella+ " SET "+colonna+"= '" +val+ "' WHERE " +colonna+" IS NULL";
				}
				st.executeUpdate(query);
				System.out.println("Operazione eseguita, blank riempiti con: "+val);
			}
		}catch(SQLException se) {
			System.out.println("Error: " + se.getMessage());
		}finally {
			DBConnection.closeSt(st);
		}
	}
	
	/*Metodo per la modifica di valori di una colonna
	 * - scelta, in base al tipo di colonna, il tipo di modifica
	 * - invocazione del rispettivo metodo 
	 */ 
	public static void modify(String colonna, String data_type, String tabella) {
		while(risp.equalsIgnoreCase("si")) {
			if(data_type.equalsIgnoreCase("string")|| data_type.equalsIgnoreCase("altro")) {
				System.out.println("Modifica possibile: input");
				inputM(colonna,tabella,data_type);
			}else if (data_type.equalsIgnoreCase("numeric")) {
				System.out.println("Inserisci tipo di modifica (operazione - input)");
				tipo = tastiera.nextLine();
				if(tipo.equalsIgnoreCase("operazione")) {
					operazione(colonna,tabella);
				}else if(tipo.equalsIgnoreCase("input")){
					inputM(colonna,tabella,data_type);
				}else {
					System.out.println("Modifica inserita non valida");
				}
			}
			System.out.println("Effettuare altre modifiche (modify) alla colonna: " +colonna+"? (si/no)");
			risp = risposta();			
		}
		System.out.println("Fine modify.");
	}
	
	/*Metodo per la modifica: OPERAZIONE
	 * - chiede all'utente l'operazione aritmetica da eseguire
	 * - chiede all'utente condizione, che permette di scegliere
	 *   i record su cui effettuare l'operazione aritmetica
	 * - effettua l'operazione
	 */ 
	public static void operazione(String colonna, String tabella) {
		String operazione = "";
		System.out.println("Inserisce l'operazione aritmetica. (es + 10)");
		operazione = tastiera.nextLine();
		System.out.println("Inserisci condizione (esempio !=1)");
		cond = tastiera.nextLine();
		try {
			query="UPDATE "+tabella+ " SET "+colonna+"="+colonna+operazione+" WHERE "+colonna+cond;
			st = DBConnection.getConn().createStatement();
			st.executeUpdate(query);
			System.out.println("Operazione "+operazione+" eseguita");
		}catch(SQLException se) {
			System.out.println("Error: " +se.getMessage());
		}finally {
			DBConnection.closeSt(st);
		}
	}
	
	/*Metodo per la modifica: INPUT
	 * - chiede all'utente il nuovo valore sostitutivo
	 * - se la colonna � di tipo numerico, chiede all'utente di inserire la condizione 
	 *   che permette di scegliere i record da modificare
	 * - altrimenti chiede esplicitamente all'utente il valore che vuole modificare
	 * - effettua la sostituzione con il nuovo valore inserito in input
	 */ 
	public static void inputM(String colonna, String tabella, String data_type) {
		System.out.println("Inserisci nuovo valore");
		val = tastiera.nextLine();
		if(data_type.equalsIgnoreCase("numeric")) {
			System.out.println("Inserisci la condizione (es =1)");
			cond = tastiera.nextLine();
			try {
				query = "UPDATE "+tabella+ " SET "+colonna+ "='"+val+"' WHERE "+colonna+cond;
				st = DBConnection.getConn().createStatement();
				st.executeUpdate(query);
				System.out.println("Update eseguito, inserito nuovo valore: "+val);
			}catch(SQLException se) {
				System.out.println("Error: "+se.getMessage());
			}finally {
				DBConnection.closeSt(st);
			}
		}else {
			System.out.println("Inserisci il valore da modificare");
			String vm = tastiera.nextLine();
			try {
				query = "UPDATE "+tabella+" SET "+colonna+"= '"+val+"' WHERE "+colonna+"='"+vm+"'";
				st = DBConnection.getConn().createStatement();
				st.executeUpdate(query);
				System.out.println("Update eseguito, inserito nuovo valore: "+val);
			}catch(SQLException se) {
				System.out.println("Error: "+se.getMessage());
			}finally {
				DBConnection.closeSt(st);
			}
		}		
	}
	//Metodo che restituisce la risposta dell'utente (si/no)
	public static String risposta() {
		boolean run=true;
		String input ="";
		while(run) {
			input = tastiera.nextLine();
			if(input.equalsIgnoreCase("si")||input.equalsIgnoreCase("no")) {
				run=false;
			}else {
				System.out.println("Attenzione, inserire si o no.");
			}
		}
		return input;
	}
	
	
	//to check if column is key
	public static boolean isKey(String column, String[] index) {
		for(int i=0; i < index.length; i++) {
			if(column.equals(index[i])) {
				return true;
			}
		}
		return false;
	}
	
	//to check column existence
	public static boolean searchColumn(String column, String table, String db) {
		try {
			DatabaseMetaData dbm = DBConnection.getConn().getMetaData();
			rs = dbm.getColumns(db, null, table, column);
			if(!rs.next()){
				System.out.println("Column not found.");
				DBConnection.closeRs(rs);
				return false;
			}
			DBConnection.closeRs(rs);
			return true;
		}catch(SQLException se){
			System.out.println("Error: "+se.getMessage());
			DBConnection.closeRs(rs);
			return false;
		}
	}
	
	//to check table existence
	public static boolean searchTable(String table, String db) {
		try {
			DatabaseMetaData dbm = DBConnection.getConn().getMetaData();
			rs = dbm.getTables(db, null, table, null);
			if(!rs.next()) {
				System.out.println("Table not found.");
				return false;
			}
			DBConnection.closeRs(rs);
			return true;
		}catch(SQLException se){
			System.out.println("Error: "+se.getMessage());
			DBConnection.closeRs(rs);
			return false;
		}
	}
	
	
	
	
	
}




