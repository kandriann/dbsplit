package DBSplit;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {

	private static Scanner input = new Scanner(System.in).useDelimiter("\n");
	private static Scanner input2 = new Scanner(System.in);
	private static boolean run;
	private static String choice = "";
	private static String table = "";
	public static String sv ="";
	public static String user = "";
	public static String password = "";
	private static int percent = 0;
	private static int percentOverlapping = 0;
	private static String tab1 = "";
	private static String tab2 = "";
	private static String splitType = "";	
	private static int numTables = 0;
	private static long time = 0;
	public static String DB1; 
	public static String DB2;
	public static String DB; //original;
	public static String DBMS; //used for connecting to DB via JDBC (e.g., oracle, mysql, sqlserver)
	
	

	public static void main(String[] args) throws Exception {			
		System.out.println("Running DBSplit...\n");		
		System.out.println("Arguments: <server> <user> <password> <input-db> <output-db1> <output-db2> <perc-split>"
				+ "<split-type> [<perc-overlapping>] <dbms>\n");
		System.out.println("Example: localhost:3306 user001 password001 testDB half1DB half2DB 30 overlapping 60 mysql\n");
		run = true;
		while(run){
			try {
				/*boolean prova = true;
				if(prova) {
					sv = "";
					user = "";			
					password = "";
					DB = "C:\\Users\\User\\Desktop\\Database.db";
					DB1 = "C:\\Users\\User\\Desktop\\test.db";
					DB2 = "C:\\Users\\User\\Desktop\\app.db";
					percent = 50;
					splitType = "disjoint";
					percentOverlapping = 50;
					DBMS = "sqlite";
				}
				else */if(args.length>0) {
					sv = args[0];
					System.out.println(sv);
					user = args[1];		
					System.out.println(user);
					password = args[2];
					System.out.println(password);
					DB = args[3];
					System.out.println(DB);
					DB1 = args[4];
					System.out.println(DB1);
					DB2 = args[5];
					System.out.println(DB2);
					percent = Integer.parseInt(args[6]);
					System.out.println(percent);
					splitType = args[7];
					System.out.println(splitType);
					if(splitType.equals("overlapping")) {
						percentOverlapping = Integer.parseInt(args[8]);
						System.out.println(percentOverlapping);
						DBMS = args[9];
						System.out.println(DBMS);
					}
					else {
						DBMS = args[8];
						System.out.println(DBMS);
					}
				}
				else {
					System.out.print("Server name (usually 'localhost:3306' for MySQL or 'SQLEXPRESS' for SQLServer, leave empty for SQLite): ");
					sv = input.nextLine();
					System.out.print("User (usually 'root' for MySQL, leave empty for SQLite): ");
					user = input.nextLine();			
					System.out.print("Password (usually '' for MySQL, leave empty for SQLite): ");
					password = input.nextLine();			
					System.out.print("Input Database (path to file.db/file.sqlite for SQLite): ");
					DB = input.nextLine();
					System.out.print("Output Database 1� half (path to file.db/file.sqlite for SQLite): ");
					DB1 = input.nextLine();
					System.out.print("Output Database 2� half (path to file.db/file.sqlite for SQLite): ");
					DB2 = input.nextLine();
					System.out.print("Split %: ");
					percent = input2.nextInt();
					System.out.print("Split type (disjoint or overlapping): ");
					splitType = input.nextLine();
					if (splitType.equalsIgnoreCase("overlapping")){
						System.out.print("Records overlapping % (0% means disjoint, 100% means equal): ");
						percentOverlapping = input2.nextInt();
					}
					System.out.print("DBMS: ");
					DBMS = input.nextLine();
				}
				//Check parameters
					if(!DBMS.equalsIgnoreCase("mysql") && !DBMS.equalsIgnoreCase("sqlserver") && !DBMS.equalsIgnoreCase("sqlite")) {
						System.out.println("Error: Invalid or non-supported DBMS");
						run = retry();
					}
					else if(percent < 0 || percent > 100) {
						System.out.println("Error: Invalid split %");
						run = retry();
					}	
					else if (!splitType.equalsIgnoreCase("disjoint") && !splitType.equalsIgnoreCase("overlapping")){
						System.out.println("Error: Invalid split type");
						run = retry();
					}
					else if(splitType.equalsIgnoreCase("overlapping") && (percentOverlapping < 0 || percentOverlapping > 100)) {
						System.out.println("Error: Invalid records in common %");
						run = retry();
					}	
					else if(!(DBConnection.setConn(DBMS, sv, user, password, DB)) || !DBSplit.createSplitDatabases(DBMS, sv, user, password, DB, DB1, DB2)) {
						System.out.println("Error: Invalid database parameters");
						run = retry();
					}
					else {
						System.out.println("\nConnection to server started successfully!\n");
						System.out.print("Entered parameteres: server=" + sv + " username="+user+ " password=" +password
								+ " original DB="+ DB + " 1� half DB=" + DB1 + " 2� half DB=" + DB2 + 
								" split type=" + splitType + " split %=" + percent); 
						if(splitType.equalsIgnoreCase(("overlapping")))
							System.out.print("overlapping%=" +percentOverlapping);
						System.out.println(" DBMS=" +DBMS);
						System.out.println("\nApplying DBSplit...\n");
						prepareSplit();
						run = false;
					}
			}
			catch(InputMismatchException e) {
				System.out.println("Error: Wrong % of split");
				input.next();
				input2.next();
				run = retry();
			}
		}
		//MUTATIONS
		/*run = true;
		risp = "";
		while(run) {
			System.out.println("Effettuare modifiche al database? (si/no)");
			risp = tastiera.nextLine();
			if(risp.equalsIgnoreCase("si")) {
				DBMutation.mutation(db);
				run = false;
			}else if(risp.equalsIgnoreCase("no")) {
				run = false;
			}
		}*/
		System.out.println("DBSplit Completed...");
		System.out.println("Summary:");
		System.out.println("DB Name: " + DB);
		System.out.println("1st Half DB Name: " + DB1);
		System.out.println("2nd Half DB Name: " + DB2);
		System.out.println("# Tables: " + numTables);
		System.out.println("Split %: " + percent + "%");
		System.out.println("Split Type: " + splitType);
		if(splitType.equalsIgnoreCase("overlapping")) {
			System.out.println("Overlapping records %: "+ percentOverlapping + "%");
		}
		System.out.println("Execution Time (s): "+(double)time/1000+" s");
		System.out.println("Errors Found: " + DBSplit.getErrors());
		DBConnection.closeConn();		
	}
	
	
	
	
	
	public static void prepareSplit() {
		ResultSet rs = null;
		Statement st = null;
		try {
			//Retrieving the meta data object (see https://stackoverflow.com/questions/2780284/how-to-get-all-table-names-from-a-database)
		    DatabaseMetaData metaData = DBConnection.getConn().getMetaData();
		    String[] types = {"TABLE"};
		    //Retrieving the columns in the database
		    //WARNING: check schema if different from default one (i.e. dbo) in case the returned table list is empty)
		    rs = metaData.getTables(DB, "dbo", "%", types);//dbo schema should work for any DBMS but has to be checked (see https://stackoverflow.com/questions/30497927/databasemetadata-gettables-returns-trace-xe-action-map-trace-xe-event-map)
		    List<String> tables = new ArrayList<String>();
		    while (rs.next()) {
		    	tables.add(rs.getString(3));//according to DatabaseMetaData.getTable() documentation, position 3 is for table name (see https://docs.oracle.com/javase/8/docs/api/java/sql/DatabaseMetaData.html#getTables-java.lang.String-java.lang.String-java.lang.String-java.lang.String:A-)
		    }
		   //WARNING: maybe it is not necessary, but dbo schema is needed by some sql server databases to access to tables
		    if(DBMS.equalsIgnoreCase("sqlserver")) {
				    	DB = DB + ".dbo";
				    	DB1 = DB1 + ".dbo";
				    	DB2 = DB2 + ".dbo";
		    }
		    for (String t: tables) {
				numTables++;
				table = t; 
				if (DBMS.equalsIgnoreCase("sqlite")) {
					DBSplit.split(table, percent, percentOverlapping);
				}
				else {
					tab1 = table;
					tab2 = table;
					if(splitType.equalsIgnoreCase("overlapping") && percentOverlapping != 0 && (percent != 0 || percent != 100)) {
						DBSplit.splitOverlapping(table, percent, percentOverlapping, tab1, tab2);
					}else {
						DBSplit.splitDisjoint(table, percent, tab1, tab2);
					}
				}
				time = time + DBSplit.getTime();
			}
		}catch (SQLException se) {
			System.out.println("Error: " + se.getMessage());
		}finally {
			DBConnection.closeSt(st);
			DBConnection.closeRs(rs);
		}
	}
	
	public static boolean retry() {
		choice="";
		do {
			System.out.println("Try again? (yes/no)");
			choice = input.nextLine();
			if (choice.equalsIgnoreCase("no")) {
				System.out.println("DBSplit aborted.");
				DBConnection.closeConn();
				System.exit(1);
				return false;
			}else if(choice.equals("yes")){
				return true;
			}
		}while (true);			
	}	
	
}


