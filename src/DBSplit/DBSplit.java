package DBSplit;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.sqlite.SQLiteException;

public class DBSplit {
	
	private static String query = "";
	private static Statement st = null;
	private static ResultSet rs = null;
	private static int totalRecords = -1;
	private static int n = -1, m = -1;
	private static int err = 0;
	private static long time = 0;//time
	
		
	
	
	//to create split output DBs
	public static boolean createSplitDatabases(String DBMS, String sv, String user, String password, String DB, String DB1, String DB2) {
		if (DBMS.equalsIgnoreCase("sqlite")) {//SQLite requires just to copy the original file
			try {
				Files.copy(Paths.get(DB), Paths.get(DB1), StandardCopyOption.REPLACE_EXISTING);
				Files.copy(Paths.get(DB), Paths.get(DB2), StandardCopyOption.REPLACE_EXISTING);
				//this operation is needed for trickytripper app only, to handle with a problematic table which is emptied
				if(DB.contains("trickytripper")) {
					DBConnection.setConn(Main.DBMS, Main.sv, Main.user, Main.password, Main.DB1);
					query = "DELETE FROM rel_payment_participant"; 
					st = DBConnection.getConn().createStatement();
					st.execute(query);
					DBConnection.setConn(Main.DBMS, Main.sv, Main.user, Main.password, Main.DB2);
					query = "DELETE FROM rel_payment_participant"; 
					st = DBConnection.getConn().createStatement();
					st.execute(query);
				}
				return true;
			} catch (Exception e) {
				System.out.println("Error: " + e.getMessage());
				return false;
			}
		}
		else {
			try {
				Statement st = DBConnection.getConn().createStatement();
				st.executeUpdate("CREATE DATABASE " + DB1);
				st.executeUpdate("CREATE DATABASE " + DB2);
				return true;
			}catch (SQLException se) {
				System.out.println("Error: " + se.getMessage());
				return false;
			}	
		}
	}
	
	public static String print(List<Integer> rowids) {
		String rowidsString = "";
		for(int i=0; i<rowids.size(); i++) 
			rowidsString += rowids.get(i) + ","; //build the query string of N rowids to remove from tab1	
		if (rowids.size()>0)
			rowidsString = rowidsString.substring(0, rowidsString.length() - 1);//remove last ','
		return rowidsString;
	}
	
	
	
	public static void split(String table, int splitPerc, int overlappingPerc) {
		long startTime = System.currentTimeMillis();
		if (table.equalsIgnoreCase("android_metadata") || table.equalsIgnoreCase("sqlite_sequence") 
				|| table.equalsIgnoreCase("cache"))//default tables are not split
			return;
		try {
			//get all records from full table
			query = "SELECT rowid FROM " + table; 
			DBConnection.setConn(Main.DBMS, Main.sv, Main.user, Main.password, Main.DB);
			st = DBConnection.getConn().createStatement();
			rs = st.executeQuery(query);
			List<Integer> fullTableRecords = new ArrayList<Integer>();			
			while(rs.next()) {
				fullTableRecords.add(rs.getInt(1));
			}
			DBConnection.closeSt(st);
			DBConnection.closeRs(rs);
			DBConnection.closeConn();
			if(fullTableRecords.size() == 0) //no split needed
				return;
			else if(fullTableRecords.size() > 0) {
					computeParameters(splitPerc, fullTableRecords.size());//compute N and M parameters for split
					Random r = new Random();
					//remove N records from tab1 
					List<Integer> recordsToRemove = new ArrayList<Integer>(); 
					for(; recordsToRemove.size()<n;) {
						int num = r.nextInt(fullTableRecords.size());
						int rowid = fullTableRecords.get(num);
						if(!recordsToRemove.contains(rowid)) 
							recordsToRemove.add(rowid);
					}
					if (recordsToRemove.size() > 0) {
						DBConnection.setConn(Main.DBMS, Main.sv, Main.user, Main.password, Main.DB1);
						try {
							query = "DELETE FROM " + table + " WHERE rowid IN (" + print(recordsToRemove) + ")"; 
							st = DBConnection.getConn().createStatement();
							st.execute(query);
						}
						catch(SQLiteException e) {
							System.out.println(e);
						}
						finally {
							DBConnection.closeSt(st);
							DBConnection.closeConn();
						}
					}
					//get k random common records with tab1 not to remove from tab2
					List<Integer> tab1Records = new ArrayList<Integer>(fullTableRecords);	
					tab1Records.removeAll(recordsToRemove);
					int k = 0;
					if(n <= m) {
						k = (int)Math.round((double)(n*overlappingPerc)/100);
					}else {
						k = (int)Math.round((double)(m*overlappingPerc)/100);
					}
					List<Integer> commonRecords = new ArrayList<Integer>();	
					for(; commonRecords.size()<k;) {
						int num = r.nextInt(tab1Records.size());//get a random number within the size of tab1
						int rowid = tab1Records.get(num);
						if(!commonRecords.contains(rowid))
							commonRecords.add(rowid);
					}
					//remove M records from tab2. from original table: 1. remove all records in tab1 2. keep common records 3. remove all records not in tab1 until M elements are removed
					recordsToRemove.clear();
					recordsToRemove.addAll(tab1Records);//1. remove all tab1
					recordsToRemove.removeAll(commonRecords);//2. do not remove common
					//3. remove additional records not in tab1 until M records to remove are found
					for(; recordsToRemove.size()<m;) {
						int num = r.nextInt(fullTableRecords.size());
						int rowid = fullTableRecords.get(num);
						if(!recordsToRemove.contains(rowid) && !tab1Records.contains(rowid)) 
							recordsToRemove.add(rowid);
					}
					if (recordsToRemove.size() > 0) {
						try {
							query = "DELETE FROM " + table + " WHERE rowid IN (" + print(recordsToRemove) + ")"; 
							DBConnection.setConn(Main.DBMS, Main.sv, Main.user, Main.password, Main.DB2);
							st = DBConnection.getConn().createStatement();
							st.execute(query);	
						}
						catch(SQLiteException e) {
							System.out.println(e);
						}
						finally {
							DBConnection.closeSt(st);
							DBConnection.closeConn();
						}
					}
			}
			else {
				System.out.println("Error: Split operation over table " + table + " not completed");
				err++;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			DBConnection.closeSt(st);
			DBConnection.closeRs(rs);
			time = System.currentTimeMillis() - startTime;
		}
	}
	
	
	
	
	
	/* disjoint split, for each table
	 * tab1: first N records of original table
	 * tab2: from N+1 to M records of original table
	 * notice that N+M = # records of original table
	 */
	public static void splitDisjoint(String table, int splitPerc, String tab1, String tab2) {
		long startTime = System.currentTimeMillis();
		computeRecords(table, Main.DB);
		if(totalRecords == 0) {
			createTable(table, tab1, "1");//1 stands for DB1
			createTable(table, tab2, "2");//2 stands for DB2			
		}else if(totalRecords > 0) {
			computeParameters(splitPerc, totalRecords);
			if(createTable(table, tab1, "1")) {insertIntoTable(table, 0, n, tab1, "1");
			}
			if(createTable(table, tab2, "2")) {insertIntoTable(table, n, m, tab2, "2");
			}
		}else {
			System.out.println("Error: Split operation over table " + table + " not completed");
			err++;
			time = System.currentTimeMillis() - startTime;
			return;
		}
		time = System.currentTimeMillis() - startTime;
	}
	
	/* overlapping (and equal) split, for each table
	 * tab1: first N records of original table
	 * tab2: first M records of original table
	 * according to commonPerc, N and M might be the same
	 * notice that N+M = # records of original table
	 */ 
	public static void splitOverlapping(String table, int splitPerc, int overlappingPerc, String tab1, String tab2) {
		long startTime = System.currentTimeMillis();
		computeRecords(table, Main.DB);
		if(totalRecords == 0) {
			createTable(table, tab1, "1");
			createTable(table, tab2, "2");			
		}else if(totalRecords > 0) {
			computeParameters(splitPerc,totalRecords);
			//compute # of records in common
			int c = -1;
			if(n <= m) {
				c = (int)Math.round((double)(n*overlappingPerc)/100);
			}else {
				c = (int)Math.round((double)(m*overlappingPerc)/100);
			}
			n = n - c;//this because we will have to add C records in common (N*percCommon or M*percCommon) + (N-C) records disjoint
			m = m - c;//this because we will have to add C records in common (N*percCommon or M*percCommon) + (M-C) records disjoint
			String tempCommonRecordsTab = "prov1";//temp table with common records
			String tempDisjointRecordsTab = "prov2";//temp table with disjoint records
			String columns = "";			
			if(createTable(table, tempCommonRecordsTab, "0")&& createTable(table, tempDisjointRecordsTab, "0")) {//temp tables are created in original DB
				insertIntoTable(table, c, tempCommonRecordsTab, "0");//C records are added in tempCommonRecordsTab
				try {
					//columns name string is created
					ResultSetMetaData md = null;
					st = DBConnection.getConn().createStatement();
					rs = st.executeQuery("SELECT* FROM " + Main.DB+"."+table);
					md = rs.getMetaData();
					int numCols = md.getColumnCount(); 
					List<String> columnNames = new ArrayList<String>();
					for(int i=1; i<=numCols; i++) {//index starts from 1
						columnNames.add(md.getColumnName(i));
					}
					columns = columnNames.get(0);
					for(int i=1; i<columnNames.size(); i++ ) {
						columns = columns + ", "+ columnNames.get(i);
					}
					System.out.println(columns);
					//other disjoint records are added in tempDisjointRecordsTab
					if(Main.DBMS.equalsIgnoreCase("mysql")) {
						query = "INSERT INTO " + Main.DB+"."+tempDisjointRecordsTab + " SELECT* FROM " +Main.DB+"."+table+ " WHERE(" + columns + ") NOT IN (SELECT " + columns + " FROM " +Main.DB+"."+tempCommonRecordsTab + ")";
						st.execute(query);					
					}//sql server does not work with NOT IN on complex queries
					else if(Main.DBMS.equalsIgnoreCase("sqlserver")) {
						String whereClause = "";
						for(String col: columnNames) {//where clause to check if TAB1.C = TAB2.C for each column C. this clause will be negated through NOT EXISTS 
							whereClause += Main.DB+"."+tempCommonRecordsTab + "." + col + " = " + Main.DB+"."+table + "." + col + " AND ";
						}
						whereClause = whereClause.substring(0, whereClause.length() - 4); //remove last AND
						query = "INSERT INTO " + Main.DB+"."+tempDisjointRecordsTab + " SELECT* FROM " +Main.DB+"."+table+ " WHERE NOT EXISTS (SELECT * FROM " +Main.DB+"."+tempCommonRecordsTab + " WHERE " + whereClause +")";
						st.execute(query);					
					}
					else 
						throw new IllegalArgumentException("DBMS not supported");
				}catch(SQLException se) {
					System.out.println("Error: "+ se.getMessage());
					System.out.println("Error: Operation over " + table + " not completed.");
					dropTable(tempCommonRecordsTab);
					dropTable(tempDisjointRecordsTab);
					err++;
					time = System.currentTimeMillis() - startTime;
					return;
				}finally {
					DBConnection.closeSt(st);
					DBConnection.closeRs(rs);
				}
				if(createTable(table, tab1, "1")) {
					try {
						//common records are added in tab1
						query = "INSERT INTO " + Main.DB1+"."+tab1 + " SELECT* FROM " + Main.DB+"."+tempCommonRecordsTab;
						st = DBConnection.getConn().createStatement();
						st.execute(query);
						//disjoint records from 0 to N are added in tab1
						insertIntoTable(tempDisjointRecordsTab, 0, n, tab1, "1");
					}catch(SQLException se) {
						System.out.println("Error: "+ se.getMessage());
						System.out.println("Error: Records in common not added in "+tab1);
					}finally {
						DBConnection.closeSt(st);
					}
				}
				if(createTable(table, tab2, "2")) {
					try {
						//common records are added in tab2
						query = "INSERT INTO " + Main.DB2+"."+tab2 + " SELECT* FROM " + Main.DB+"."+tempCommonRecordsTab;
						st = DBConnection.getConn().createStatement();
						st.execute(query);
						//disjoint records from N+1 to M are added in tab2
						insertIntoTable(tempDisjointRecordsTab, n, m, tab2, "2");	
					}catch(SQLException se) {
						System.out.println("Error: "+ se.getMessage());
						System.out.println("Error: Records in common not added in "+tab2);
					}finally {
						DBConnection.closeSt(st);
					}					
				}
				dropTable(tempCommonRecordsTab);
				dropTable(tempDisjointRecordsTab);				
			}			
		}else {
			System.out.println("Error: Split operation not completed.");
			err++;
			time = System.currentTimeMillis() - startTime;
			return;
		}
		time = System.currentTimeMillis() - startTime;
	}
	
	
	
	

	
	
	
	/* to compute N and M parameters indicating quantity of desired records
	 * The computation is based on the split percentage and total number of records in the table
	 */
	public static void computeParameters(int percentage, int tot_record) {
		n = (int)Math.round((double)(tot_record*percentage)/100);
		m = tot_record - n;		
	}
	
	//to return total number of records in a table
	public static void computeRecords(String table, String DB) {
		try {
			query = "SELECT count(*) FROM " + DB + "."+table;
			st = DBConnection.getConn().createStatement();
			rs = st.executeQuery(query);
			if(rs.next()) {
				totalRecords = rs.getInt(1);
			}
			DBConnection.closeRs(rs);
			DBConnection.closeSt(st);			
		}catch(SQLException se) {
			System.out.println("Error: " + se.getMessage());
			DBConnection.closeRs(rs);
			DBConnection.closeSt(st);
		}
	}
		
	/* it creates a new table keeping original referencial integrity 
	 * DBnum identifies the DB number to split (1st or 2nd half)
	 */	
	public static boolean createTable(String table, String newTable, String DBnum) {
		try {
			if(DBnum != "0" && DBnum != "1" && DBnum != "2")
				throw new SQLException("Database number is wrong");
			if(Main.DBMS.equalsIgnoreCase("mysql")) {
				if(DBnum.equals("0")) {
					query = "CREATE TABLE " + Main.DB+"."+newTable + " LIKE " + Main.DB+"."+table;
				}
				else if(DBnum.equals("1")) {
					query = "CREATE TABLE " + Main.DB1+"."+newTable + " LIKE " + Main.DB+"."+table;
				}
				else {
					query = "CREATE TABLE "+  Main.DB2+"."+newTable + " LIKE " + Main.DB+"."+table;
				}
				st = DBConnection.getConn().createStatement();
				st.execute(query);
			}
			//sql server does not work with LIKE
			else if(Main.DBMS.equalsIgnoreCase("sqlserver")) {
				String query1 = "";
				//to create an empty table B with the schema of A it is needed to move first the rows from A to B and then remove everything from B
				if(DBnum.equals("0")) {
					query = "SELECT * INTO " + Main.DB +"."+newTable + " FROM " + Main.DB+"."+table;
					query1 = "DELETE FROM " + Main.DB +"."+newTable;
				}
				else if(DBnum.equals("1")) {
					query = "SELECT * INTO " + Main.DB1 +"."+newTable + " FROM " + Main.DB+"."+table;
					query1 = "DELETE FROM " + Main.DB1 +"."+newTable;
				}
				else {
					query = "SELECT * INTO " + Main.DB2 +"."+newTable + " FROM " + Main.DB+"."+table;
					query1 = "DELETE FROM " + Main.DB2 +"."+newTable;
				}
				st = DBConnection.getConn().createStatement();
				st.execute(query);
				st = DBConnection.getConn().createStatement();
				st.execute(query1);
			}
			else 
				throw new IllegalArgumentException("DBMS not supported");
			DBConnection.closeSt(st);
			return true;
		}catch(SQLException se) {
			System.out.println("Error: " + se.getMessage());
			System.out.println("Error: Table " + newTable + " has not been created");
			err++;
			DBConnection.closeSt(st);
			return false;
		}
	}

	/* it inserts into a table (newTable) N/M records from original table given a starting record number to add from
	 */
	public static void insertIntoTable(String table, int n, int m, String newTable, String DBnum) {
		try {
			if(DBnum != "0" && DBnum != "1" && DBnum != "2")
				throw new SQLException("Database number is wrong");
			if(Main.DBMS.equalsIgnoreCase("mysql")) {
				if(DBnum.equals("0"))
					query = "INSERT INTO "+ Main.DB+"."+newTable +" SELECT* FROM "+ Main.DB+"."+table + " LIMIT "+ n +","+ m;
				else if(DBnum.equals("1"))
					query = "INSERT INTO "+ Main.DB1+"."+newTable +" SELECT* FROM "+ Main.DB+"."+table + " LIMIT "+ n +","+ m;
				else
					query = "INSERT INTO "+ Main.DB2+"."+newTable +" SELECT* FROM "+ Main.DB+"."+table + " LIMIT "+ n +","+ m;
				st = DBConnection.getConn().createStatement();
				st.execute(query);		
			}
			//sql server does not work with LIMIT therefore it is needed to order first by primary keys and then take the records using FETCH
			else if(Main.DBMS.equalsIgnoreCase("sqlserver")) {
				if(m<=0)//this to avoid to insert in empty table, since FETCH command arises problems when M<=0
					return;
			    String db = Main.DB.split("\\.")[0]; //since we have DB.dbo
			    //find primary keys 
		        String primaryKeysQuery = " ORDER BY ";
			    DatabaseMetaData metaData = DBConnection.getConn().getMetaData();
				try (ResultSet tables = metaData.getTables(db, "dbo", "%", new String[] { "TABLE" })) {
				    while (tables.next()) {
				    	if(!tables.getString("TABLE_NAME").equals(table))
				    		continue;
				        String catalog = tables.getString("TABLE_CAT");
				        String schema = tables.getString("TABLE_SCHEM");
				        String tableName = tables.getString("TABLE_NAME");
				        int numPrimaryKeys = 0; //this is used in case of tables with no primary keys. in that case, newid() is used instead
				        try (ResultSet primaryKeys = metaData.getPrimaryKeys(catalog, schema, tableName)) {
				            while (primaryKeys.next()) {
				            	numPrimaryKeys++;
				                primaryKeysQuery += primaryKeys.getString("COLUMN_NAME") + ",";
				            }
				            if(numPrimaryKeys == 0)
				            	primaryKeysQuery = " ORDER BY newid() "; 
				            else
				            	primaryKeysQuery = primaryKeysQuery.substring(0, primaryKeysQuery.length() - 1); //remove last ,
				        }
				    }
				}
				if(DBnum.equals("0"))
					query = "INSERT INTO "+ Main.DB+"."+newTable +" SELECT* FROM "+ Main.DB+"."+table + 
							primaryKeysQuery + " OFFSET " + n + " ROWS FETCH NEXT " + m + " ROWS ONLY";
				else if(DBnum.equals("1"))
					query = "INSERT INTO "+ Main.DB1+"."+newTable +" SELECT* FROM "+ Main.DB+"."+table + 
					primaryKeysQuery + " OFFSET " + n + " ROWS FETCH NEXT " + m + " ROWS ONLY";				
				else
					query = "INSERT INTO "+ Main.DB2+"."+newTable +" SELECT* FROM "+ Main.DB+"."+table + 
					primaryKeysQuery + " OFFSET " + n + " ROWS FETCH NEXT " + m + " ROWS ONLY";				
				st = DBConnection.getConn().createStatement();
				st.execute(query);	
			}
			else 
				throw new IllegalArgumentException("DBMS not supported");
		}catch(SQLException se) {
			System.out.println("Error: " + se.getMessage());
			System.out.println("Records in table " + newTable + " have not been inserted");
			dropTable(newTable);
			err++;
		}finally {
			DBConnection.closeSt(st);
		}
	}
	
	/* it insert into a table (newTable) N random records from original table
	 */
	public static void insertIntoTable(String table, int n, String newTable, String DBnum) {
		try {
			if(DBnum != "0" && DBnum != "1" && DBnum != "2")
				throw new SQLException("Database number is wrong");
			if(Main.DBMS.equalsIgnoreCase("mysql")) {
				if(DBnum.equals("0"))
					query = "INSERT INTO "+ Main.DB+"."+newTable +" SELECT* FROM "+ Main.DB+"."+table + " ORDER BY RAND() LIMIT "+ n;
				else if(DBnum.equals("1"))
					query = "INSERT INTO "+ Main.DB1+"."+newTable +" SELECT* FROM "+ Main.DB+"."+table + " ORDER BY RAND() LIMIT "+ n;
				else
					query = "INSERT INTO "+ Main.DB2+"."+newTable +" SELECT* FROM "+ Main.DB+"."+table + " ORDER BY RAND() LIMIT "+ n;
				st = DBConnection.getConn().createStatement();
				st.execute(query);		
			}
			//sql server does not work with LIMIT
			else if(Main.DBMS.equalsIgnoreCase("sqlserver")) {
				if(n<=0)//this to avoid insert in empty table, since FETCH command arise problems when N<=0
					return;
				if(DBnum.equals("0"))
					query = "INSERT INTO "+ Main.DB+"."+newTable +" SELECT* FROM "+ Main.DB+"."+table + 
							" ORDER BY newid() OFFSET 0 ROWS FETCH NEXT " + n + " ROWS ONLY";
				else if(DBnum.equals("1"))
					query = "INSERT INTO "+ Main.DB1+"."+newTable +" SELECT* FROM "+ Main.DB+"."+table + 
					" ORDER BY newid() OFFSET 0 ROWS FETCH NEXT " + n + " ROWS ONLY";		
				else
					query = "INSERT INTO "+ Main.DB2+"."+newTable +" SELECT* FROM "+ Main.DB+"."+table + 
					" ORDER BY newid() OFFSET 0 ROWS FETCH NEXT " + n + " ROWS ONLY";	
				st = DBConnection.getConn().createStatement();
				st.execute(query);	
			}
			else 
				throw new IllegalArgumentException("DBMS not supported");
		}catch(SQLException se) {
			System.out.println("Error: " + se.getMessage());
			System.out.println("Records in table " + newTable + " have not been inserted");
			dropTable(newTable);
			err++;
		}finally {
			DBConnection.closeSt(st);
		}
	}
	
	
	//to drop table
	public static void dropTable(String table) {
		Statement st = null;
		try {
			String query = "DROP TABLE " + Main.DB+"."+table;
			st = DBConnection.getConn().createStatement();
			st.executeUpdate(query);
		}catch (SQLException se) {
			System.out.println("Error: " + se.getMessage());
		}finally {
			DBConnection.closeSt(st);
		}
	}
	
	
	
	
	
	
	
	
	
	
	public static int getErrors() {
		return err;
	}
	public static long getTime() {
		return time;
	}

}
